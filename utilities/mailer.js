var nodemailer = require('nodemailer');
var hbs = require('nodemailer-express-handlebars');
var options = {
    viewEngine: {
        extname: '.hbs',
        layoutsDir: 'views/email/',
        defaultLayout : 'template'
    },
    viewPath: 'views/email/',
    extName: '.hbs'
};

var transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
        user: 'storegame786@gmail.com',
        pass: 'storegame786storegame786'
    }
});

transporter.use('compile', hbs(options));

module.exports.send = function (email, link, text) {
    var mailOptions = {
        from: 'storegame786@gmail.com',
        to: email,
        subject: 'Activate your account',
        template: 'email-body',
        context: {
            link: link,
            text: text
        }
    };

    transporter.sendMail(mailOptions, function(error, info){
        if (error) {
            console.log(error);
        } else {
            console.log('Email sent: ' + info.response);
        }
    });
};
function predicateBy(criteria) {
    return function (a, b) {
        if (a[criteria] > b[criteria]) {
            return 1;
        } else if (a[criteria] < b[criteria]) {
            return -1;
        }
        return 0;
    }
}


module.exports.getTop10 = function (orders) {
    var occurrences = [];
    var iTitle;
    for (i = 0, ilen = orders.length; i < ilen; i++) {
        for (var j in orders[i].cart.items) {
            if (occurrences.length < 1) {
                occurrences.push({
                    id: orders[i].cart.items[j].item._id,
                    title: orders[i].cart.items[j].item.title,
                    count: orders[i].cart.items[j].qty
                })
            } else for (k = 0, klen = occurrences.length; k < klen; k++) {
                if (occurrences[k].id === orders[i].cart.items[j].item._id) {
                    occurrences[k].count += orders[i].cart.items[j].qty;
                } else {
                    occurrences.push({
                        id: orders[i].cart.items[j].item._id,
                        title: orders[i].cart.items[j].item.title,
                        count: orders[i].cart.items[j].qty
                    })
                }
            }
        }
    }
    occurrences.sort(predicateBy("count"));
    occurrences.slice(0, 10);
    return occurrences;
};
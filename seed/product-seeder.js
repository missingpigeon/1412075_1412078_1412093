var mongoose = require('mongoose');
mongoose.connect('localhost:27017/shopping');
var Product = require('../model/product');

var products = [
    new Product({
        imagePath: 'https://torrentsgames.net/wp-content/uploads/2016/07/Rocket-League.jpg',
        title: 'Rocket League',
        description: 'A futuristic Sports-Action game, Rocket League® equips players with booster-rigged vehicles that can be crashed into balls for incredible goals or epic saves across multiple, highly-detailed arenas. Using an advanced physics system to simulate realistic interactions, Rocket League relies on mass and momentum to give players a complete sense of intuitive control in this unbelievable, high-octane re-imagining of association football.',
        price: 19.99,
        genre: ['Sports'],
        platform: 'PC',
        quantity: 12
    }),
    new Product({
        imagePath: 'http://cdn4.dualshockers.com/wp-content/uploads/2016/02/ARK-Survival-Evolved.jpg',
        title: 'ARK: Survival Evolved',
        description: 'As a man or woman stranded naked, freezing and starving on the shores of a mysterious island called ARK, you must hunt, harvest resources, craft items, grow crops, research technologies, and build shelters to withstand the elements. Use your cunning and resources to kill or tame & breed the leviathan dinosaurs and other primeval creatures roaming the land, and team up with or prey on hundreds of other players to survive, dominate... and escape!',
        price: 29.99,
        genre: ['Action','Adventure','RPG'],
        platform: 'PC',
        quantity: 25
    }),
    new Product({
        imagePath: 'http://cdn1.expertreviews.co.uk/sites/expertreviews/files/1/31/lasst_of_us_remastered_0.jpg',
        title: 'The Last of Us Remastered',
        description: 'Winner of over 200 Game of the Year awards, The Last of Us™ has been rebuilt for the PlayStation®4 system. Now featuring full 1080p, higher resolution character models, improved shadows and lighting, in addition to several other gameplay improvements.',
        price: 59.99,
        genre: ['Action','Adventure'],
        platform: 'PS4',
        quantity: 19
    }),
    new Product({
        imagePath: 'http://igeekout.net/wp-content/uploads/2016/06/What-is-Bethesda-Planning-Pic-1.jpg',
        title: 'The Elder Scrolls III: Morrowind',
        description: 'An epic, open-ended single-player RPG, Morrowind allows you to create and play any kind of character imaginable. You can choose to follow the main storyline and find the source of the evil blight that plagues the land, or set off on your own to explore strange locations and develop your character based on their actions throughout the game. Featuring stunning 3D graphics, open-ended gameplay, and an incredible level of detail and interactivity, Morrowind offers a gameplay experience like no other. ',
        price: 14.99,
        genre: ['RPG'],
        platform: 'PC',
        quantity: 50
    }),
    new Product({
        imagePath: 'https://image.halocdn.com/?path=https:%2F%2Fcontent.halocdn.com%2Fmedia%2FDefault%2Fgames%2Fhalo-5-guardians%2Fheroes%2Fhalo5-large-hero-1920x675-de822bb343a84943a22b87d95f95cd7c.jpg&leftCrop=400&rightCrop=400&hash=zWftjFsm38xGdJo8FYCTLBpPmf4cS4Siw0CdMem%2FFR8%3D',
        title: 'Halo 5: Guardians',
        description: 'Halo 5: Guardians delivers epic multiplayer experiences that span multiple modes, full-featured level building tools, and the most dramatic Halo story to date. With the release of 10 massive FREE content releases since launch, Halo 5: Guardians offers more content, more multiplayer mayhem, and more variety than any Halo ever released!',
        price: 39.99,
        genre: ['Action'],
        platform: 'Xbox One',
        quantity: 8
    }),
    new Product({
        imagePath: 'https://i.ytimg.com/vi/kLtZXu8eGZ4/maxresdefault.jpg',
        title: 'Vanquish',
        description: 'From master director Shinji Mikami of Resident Evil fame, SEGA brings PlatinumGames’ revolutionary sci-fi action shooter to PC. Unlocked framerate, unlocked HD resolutions, extensive graphics options: the definitive way to play. War has accelerated.',
        price: 19.99,
        genre: ['Action'],
        platform: 'PC',
        quantity: 29
    }),
    new Product({
        imagePath: 'https://i.ytimg.com/vi/V24ONWLpVmY/maxresdefault.jpg',
        title: 'Titanfall 2',
        description: 'From Respawn Entertainment comes Titanfall® 2. Featuring the debut of a single player campaign and a deeper multiplayer experience, Titanfall® 2 delivers more of the innovative, fast, fluid gameplay fans expect from the series.',
        price: 39.99,
        genre: ['Action'],
        platform: 'PS4',
        quantity: 34
    }),
    new Product({
        imagePath: 'http://compass.xbox.com/assets/7f/82/7f82d5e6-16f5-4a08-8d2a-e14523ecc554.jpg?n=Dishonored-2_hero-desktop_1920x720_03.jpg',
        title: 'Dishonored 2',
        description: 'Dishonored 2 is set 15 years after the Lord Regent has been vanquished and the dreaded Rat Plague has passed into history. An otherworldly usurper has seized Empress Emily Kaldwin’s throne, leaving the fate of the Isles hanging in the balance. As Emily or Corvo, travel beyond the legendary streets of Dunwall to Karnaca, the once-dazzling coastal city that holds the keys to restoring Emily to power. Armed with the Mark of the Outsider and powerful new abilities, track down your enemies and take back what’s rightfully yours.',
        price: 39.99,
        genre: ['Stealth'],
        platform: 'PC',
        quantity: 10
    }),
    new Product({
        imagePath: 'https://eteknix-eteknixltd.netdna-ssl.com/wp-content/uploads/2016/08/deus-ex-mankind.jpg',
        title: 'Deus Ex: Mankind Divided',
        description: 'Now an experienced covert operative, Adam Jensen is forced to operate in a world that has grown to despise his kind. Armed with a new arsenal of state-of-the-art weapons and augmentations, he must choose the right approach, along with who to trust, in order to unravel a vast worldwide conspiracy.',
        price: 59.99,
        genre: ['Action','RPG'],
        platform: 'PC',
        quantity: 4
    }),
    new Product({
        imagePath: 'https://assets.vg247.com/current//2016/07/xcom-2.jpg',
        title: 'XCOM 2',
        description: 'Earth has changed. Twenty years have passed since world leaders offered an unconditional surrender to alien forces. XCOM, the planet’s last line of defense, was left decimated and scattered. Now, in XCOM 2, the aliens rule Earth, building shining cities that promise a brilliant future for humanity on the surface, while concealing a sinister agenda and eliminating all who dissent from their new order. ',
        price: 39.99,
        genre: ['Strategy'],
        platform: 'PS4',
        quantity: 32
    }),
    new Product({
        imagePath: 'https://farm6.staticflickr.com/5624/23815901722_4d1edf4ed1_b.jpg',
        title: "Uncharted 4: A Thief's End",
        description: 'Several years after his last adventure, retired fortune hunter, Nathan Drake, is forced back into the world of thieves.  With the stakes much more personal, Drake embarks on a globe-trotting journey in pursuit of a historical conspiracy behind a fabled pirate treasure.  His greatest adventure will test his physical limits, his resolve, and ultimately what he’s willing to sacrifice to save the ones he loves.',
        price: 39.99,
        genre: ['Action','Adventure'],
        platform: 'PS4',
        quantity: 20
    }),
    new Product({
        imagePath: 'http://compass.xbox.com/assets/d9/1a/d91a7a1a-197a-41d6-8f25-0e7094631058.jpg?n=Xbox-One-S-GOW-4_Blade_1600x700.jpg',
        title: 'Gears of War 4',
        description: 'A new saga begins for one of the most acclaimed video game franchises in history. After narrowly escaping an attack on their village, JD Fenix and his friends, Kait and Del, must rescue the ones they love and discover the source of a monstrous new enemy. ',
        price: 59.99,
        genre: ['Action'],
        platform: 'Xbox One',
        quantity: 11
    }),
    new Product({
        imagePath: 'https://i.ytimg.com/vi/n7q_Ds1Y6YM/maxresdefault.jpg',
        title: 'Warhammer 40,000: Dawn of War III',
        description: 'Step into a brutal battle between three warring factions. In Dawn of War III you will have no choice but to face your foes when a catastrophic weapon is found on the mysterious world of Acheron. With war raging and the planet under siege by the armies of greedy Ork warlord Gorgutz, ambitious Eldar seer Macha, and mighty Space Marine commander Gabriel Angelos, supremacy must ultimately be suspended for survival.',
        price: 59.99,
        genre: ['Strategy'],
        platform: 'PC',
        quantity: 4
    }),
    new Product({
        imagePath: 'https://i.ytimg.com/vi/4_FcybvR6rA/maxresdefault.jpg',
        title: 'DOOM',
        description: 'DOOM® returns as a brutally fun and challenging modern-day shooter experience. Relentless demons, impossibly destructive guns, and fast, fluid movement provide the foundation for intense, first-person combat.',
        price: 59.99,
        genre: ['Action'],
        platform: 'PC',
        quantity: 9
    }),
    new Product({
        imagePath: 'http://cdn.akamai.steamstatic.com/steam/apps/219990/header.jpg?t=1482205619',
        title: 'Grim Dawn',
        description: 'Enter an apocalyptic fantasy world where humanity is on the brink of extinction, iron is valued above gold and trust is hard earned. This ARPG features complex character development, hundreds of unique items, crafting and quests with choice & consequence.',
        price: 24.99,
        genre: ['RPG'],
        platform: 'PC',
        quantity: 12
    }),
    new Product({
        imagePath: 'https://forums.terraria.org/index.php?attachments/terraria-png.166662/',
        title: 'Terraria',
        description: 'Dig, fight, explore, build! Nothing is impossible in this action-packed adventure game. Four Pack also available! ',
        price: 9.99,
        genre: ['Adventure'],
        platform: 'PC',
        quantity: 13
    }),
    new Product({
        imagePath: 'https://i.ytimg.com/vi/21lTnUAm-_E/maxresdefault.jpg',
        title: 'Half-Life 2',
        description: 'The player again picks up the crowbar of research scientist Gordon Freeman, who finds himself on an alien-infested Earth being picked to the bone, its resources depleted, its populace dwindling. Freeman is thrust into the unenviable role of rescuing the world from the wrong he unleashed back at Black Mesa. And a lot of people he cares about are counting on him.',
        price: 9.99,
        genre: ['Action'],
        platform: 'PC',
        quantity: 2
    }),
    new Product({
        imagePath: 'https://www.digiseller.ru/preview/310971/p1_2232443_afb2f86d.jpg',
        title: 'Counter-Strike: Global Offensive',
        description: 'Counter-Strike: Global Offensive (CS: GO) will expand upon the team-based action gameplay that it pioneered when it was launched 14 years ago. CS: GO features new maps, characters, and weapons and delivers updated versions of the classic CS content (de_dust, etc.).',
        price: 14.99,
        genre: ['Action'],
        platform: 'PC',
        quantity: 8
    }),
    new Product({
        imagePath: 'https://images-1.gog.com/e76ef4db2728146be75f28249bf5dde2a6b36ba064c07869e4c21542a1fed182.jpg',
        title: 'Stardew Valley',
        description: 'You\'ve inherited your grandfather\'s old farm plot in Stardew Valley. Armed with hand-me-down tools and a few coins, you set out to begin your new life. Can you learn to live off the land and turn these overgrown fields into a thriving home?',
        price: 14.99,
        genre: ['Adventure'],
        platform: 'PS4',
        quantity: 25
    }),
    new Product({
        imagePath: 'http://horrorworld.org/wp-content/uploads/2016/08/state-decay-year-one-survival-edition.jpg',
        title: 'State of Decay: Year One Survival Edition',
        description: 'Overcome the zombie hordes in the State of Decay: Year One Survival Edition. The original game is included, along with both DLC packs remastered in 1080p. You\'ll also get a ton of new features—weapons, playable characters, mission types, and, exclusively on Xbox One, new achievements and game DVR.',
        price: 29.99,
        genre: ['Action','RPG'],
        platform: 'Xbox One',
        quantity: 20
    }),
    new Product({
        imagePath: 'https://images-4.gog.com/08ecff4e7f0cf8b0041d8e5fc32a5d45bf6da3587c21d4d12eac815d18cffb6b.jpg',
        title: 'Ori and the Blind Forest: Definitive Edition',
        description: 'Ori and the Blind Forest tells the tale of a young orphan destined for heroics, through a visually stunning Action-Platformer crafted by Moon Studios.',
        price: 19.99,
        genre: ['Adventure'],
        platform: 'PC',
        quantity: 32
    }),
    new Product({
        imagePath: 'http://media.moddb.com/images/games/1/50/49864/maxresdefault.jpg',
        title: 'Stellaris',
        description: 'Explore a vast galaxy full of wonder! Paradox Development Studio, makers of the Crusader Kings and Europa Universalis series presents Stellaris, an evolution of the grand strategy genre with space exploration at its core.',
        price: 39.99,
        genre: ['Strategy'],
        platform: 'PC',
        quantity: 10
    }),
    new Product({
        imagePath: 'https://nmswp.azureedge.net/wp-content/themes/nomanssky/img/poster.jpg',
        title: 'No Man\'s Sky',
        description: 'Inspired by the adventure and imagination that we love from classic science-fiction, No Man\'s Sky presents you with a galaxy to explore, filled with unique planets and lifeforms, and constant danger and action.',
        price: 59.99,
        genre: ['Adventure'],
        platform: 'PS4',
        quantity: 45
    }),
    new Product({
        imagePath: 'https://howlongtobeat.com/gameimages/38029_Nier_Automata.jpg',
        title: 'NieR:Automata',
        description: 'NieR: Automata tells the story of androids 2B, 9S and A2 and their battle to reclaim the machine-driven dystopia overrun by powerful machines.',
        price: 59.99,
        genre: ['Action','Adventure'],
        platform: 'PC',
        quantity: 5
    }),
    new Product({
        imagePath: 'http://www.pixel.tv/wp-content/uploads/fallout-4-1.png',
        title: 'Fallout 4',
        description: 'Bethesda Game Studios, the award-winning creators of Fallout 3 and The Elder Scrolls V: Skyrim, welcome you to the world of Fallout 4 – their most ambitious game ever, and the next generation of open-world gaming.',
        price: 29.99,
        genre: ['RPG'],
        platform: 'PC',
        quantity: 9
    }),
    new Product({
        imagePath: 'https://staticdelivery.nexusmods.com/mods/110/images/78518-0-1473113067.jpg',
        title: 'The Elder Scrolls V: Skyrim',
        description: 'Skyrim, the fifth game in the Elder Scrolls series, takes place in the province of Skyrim 200 years after the Oblivion Crisis. Dragon Shouts play an integral role in the story as you discover your fate as the Last Dragonborn.',
        price: 39.99,
        genre: ['RPG'],
        platform: 'PC',
        quantity: 4
    })
];
var done = 0;
for (var i = 0; i < products.length; i++) {
    products[i].save(function (err, result) {
            done++;
            if (done === products.length) {
                exit();
            }
        }
    );
}

function exit() {
    mongoose.disconnect();
}


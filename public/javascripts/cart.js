$('a.add-link').click(function (e) {
    var addressValue = this.href;
    $.get(addressValue,function (totalQty) {
        $('#cartQuantity').html(totalQty);
    });
    e.preventDefault();
});

$('a.reduce-link').click(function (e) {
    var addressValue = this.href;
    var qty = $(this).parents('div').siblings('span.product-qty');
    var row = $(this).parents('li.list-group-item');
    $.get(addressValue, function () {
        var newQty = qty.html() - 1;
        if(newQty > 0){
            qty.html(newQty);
        } else {
            row.remove();
        }
        if($('ul.list-group').children().length === 0){
            $(location).attr('href', '/shopping-cart');
        }
    });
    e.preventDefault();
});

$('a.remove-link').click(function (e) {
    var addressValue = this.href;
    var row = $(this).parents('li.list-group-item');
    $.get(addressValue, function () {
        row.remove();
        if($('ul.list-group').children().length === 0){
            $(location).attr('href', '/shopping-cart');
        }
    });
    e.preventDefault();
});

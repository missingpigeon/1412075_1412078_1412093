$("#commentButton").click(function () {
    var parameters = {
        name: $('#commentName').val(),
        content: $('#commentContent').val(),
        product: $('#productId').val()
    };
    $.get('/comment', parameters, function (data) {
        /*if(!data.imagePath){
            data.imagePath = 'https://cdn4.iconfinder.com/data/icons/eldorado-user/40/user-512.png';
        }*/
        $('#commentSection').append('<div class="panel panel-default"><div class="panel-heading"><span class="commenterImage"><img src="'+ data.imagePath+'" /> </span> <h4 class="commenterName">'+ data.name +'</h4> </div> <div class="panel-body"> <div> <p class="commentText">' + data.content + '</p> <span class="date sub-text">'+ data.date+'</span> </div> </div> </div>');
        $('#commentName').val('');
        $('#commentContent').val('');
    });
});

/*$('#next').click(function () {
    var parameters = {
        currentPage: $('#currentPage') + 1
    };
    $.get('/comment/next', parameters, function (comments) {
        $('#currentPage').val(currentPage);
        $('#commentSection').empty();
        comments.forEach(function (comment) {
            $('#commentSection').append('<div class="panel panel-default"><div class="panel-heading"><span class="commenterImage"><img src="'+ comment.imagePath +'" /> </span> <h4 class="commenterName">'+ comment.name +'</h4> </div> <div class="panel-body"> <div> <p class="commentText">' + comment.content + '</p> <span class="date sub-text">'+ comment.date+'</span> </div> </div> </div>');
        });
    });
});*/

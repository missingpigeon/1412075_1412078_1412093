var express = require('express');
var router = express.Router();

var Cart = require('../model/cart');
var Order = require('../model/order');
var Product = require('../model/product');
var Card = require('../model/card');
var Comment = require('../model/comment');
/* GET home page. */
router.get('/', function (req, res, next) {
    if(req.isAuthenticated()){
        if(req.user.role === 'admin'){
            return res.redirect('/admin/dashboard');
        }
    }
    var successMsg = req.flash('success')[0];
    var searchProduct = {
        name: req.flash('productSearchName')[0],
        genre: req.flash('productSearchGenre'),
        platform: req.flash('productSearchPlatform')[0]
    };

    Product.find(function (err, docs) {
        console.log("Product found");
        var platformList = getPlatformList(docs);
        var genreList = getGenreList(docs);

        //Find product by name
        if (searchProduct.name !== undefined) {
            for (var j = docs.length - 1; j >= 0; j--) {
                if (docs[j].title.indexOf(searchProduct.name) === -1) {
                    docs.splice(j, 1);
                }
            }
        }
        //Find product by platform
        if (searchProduct.platform !== undefined && searchProduct.platform !== "All") {
            for (var j = docs.length - 1; j >= 0; j--) {
                if (docs[j].platform !== searchProduct.platform) {
                    docs.splice(j, 1);
                }
            }
        }

        //Find product by genre
        if (searchProduct.genre.length != 0) {
            for (var j = docs.length - 1; j >= 0; j--) {
                var count = 0;
                docs[j].genre.forEach(function (g) {
                    searchProduct.genre.forEach(function (gChild) {
                        if(g.indexOf(gChild) !== -1){
                            count++;
                        }
                    })
                });
                if (count !== searchProduct.genre.length) {
                    docs.splice(j, 1);
                }
            }
        }

        var productChunks = [];
        var chunkSize = 3;
        for (var i = 0; i < docs.length; i += chunkSize) {
            productChunks.push(docs.slice(i, i + chunkSize));
        }

        var notAdmin = true;
        if (req.user) {
            if (req.user.role === 'admin') {
                notAdmin = false;
            }
        }

        res.render('shop/index', {
            products: productChunks, successMsg: successMsg, noMessage: !successMsg,
            searchProduct: searchProduct, platformList: platformList, genreList: genreList
        });
    });
});

router.post('/', function (req, res, next) {
    req.flash('productSearchName', req.body.productSearchName);
    req.flash('productSearchGenre', req.body.productSearchGenre);
    req.flash('productSearchPlatform', req.body.productSearchPlatform);
    res.redirect('/');
});

router.get('/add-to-cart/:id', notAdmin, function (req, res, next) {
    var productId = req.params.id;
    var cart = new Cart(req.session.cart ? req.session.cart : {});

    Product.findById(productId, function (err, product) {
        if (err) {
            return res.redirect('/');
        }
        cart.add(product, productId);
        req.session.cart = cart;
        //console.log(req.session.cart);
        //res.redirect('/');
        res.send(req.session.cart.totalQty.toString());
    });
});

router.get('/reduce/:id', notAdmin, function (req, res, next) {
    var productId = req.params.id;
    var cart = new Cart(req.session.cart ? req.session.cart : {});
    cart.reduceByOne(productId);
    req.session.cart = cart;
    res.send();
    /*res.redirect('/shopping-cart');*/
});

router.get('/details/:id', function (req, res, next) {
    var productId = req.params.id;
    Product.findOne({_id: productId}, function (err, product) {
        if (err) {
            return res.write('Error!');
        }
        Product.find({genre: {"$in": product.genre}}, function (err, similarProducts) {
            for (var i = 0; i < similarProducts.length; i++) {
                if (similarProducts[i]._id.equals(product._id)) {
                    similarProducts.splice(i, 1);
                }
            }
            Comment.find({product: product}, function (err, comments) {
                if (err) {
                    return res.write('Error!');
                }
                /*var commentChunks = [];
                var chunkSize = 5;
                for (var i = 0; i < comments.length; i += chunkSize) {
                    commentChunks.push(comments.slice(i, i + chunkSize));
                }*/
                res.render('shop/product-details', {
                    product: product,
                    similarProducts: similarProducts,
                    comments: comments,
                    currentPage: 0
                });
            });
        });
    });
});

router.get('/comment', function (req, res, next) {
    var name = req.query.name;
    var content = req.query.content;
    var product = req.query.product;
    var comment = new Comment();
    comment.name = name;
    comment.product = product;
    comment.content = content;
    comment.date = comment.getDate();
    comment.imagePath = 'https://cdn4.iconfinder.com/data/icons/eldorado-user/40/user-512.png';
    if (req.user) {
        comment.name = req.user.firstName + ' ' + req.user.lastName;
        comment.user = req.user._id;
        comment.imagePath = req.user.imagePath;
    }
    console.log(comment);
    comment.save(function (err, result) {
        if (err) {
            console.log('Error!');
        }
        res.send(comment);
    });
});

router.get('/remove/:id', notAdmin, function (req, res, next) {
    var productId = req.params.id;
    var cart = new Cart(req.session.cart ? req.session.cart : {});
    cart.removeItem(productId);
    req.session.cart = cart;
    res.send();

});

router.get('/shopping-cart', notAdmin, function (req, res, next) {
    if (!req.session.cart) {
        return res.render('shop/shopping-cart', {products: null});
    }
    var cart = new Cart(req.session.cart);
    res.render('shop/shopping-cart', {products: cart.generateArray(), totalPrice: Math.floor(cart.totalPrice)});
});

router.get('/checkout', isLoggedIn, function (req, res, next) {
    if (!req.session.cart) {
        return res.redirect('/shopping-cart');
    }
    var cart = new Cart(req.session.cart);
    var errMsg = req.flash('error')[0];
    Card.findById(req.user.cardId, function (err, card) {
        if (err) {
            req.session.oldUrl = req.url;
            return res.redirect('/user/update-card');
        }
        res.render('shop/checkout', {total: Math.floor(cart.totalPrice), errMsg: errMsg, noError: !errMsg, card: card});
    });
});

router.post('/checkout', notAdmin, isLoggedIn, function (req, res, next) {
    if (!req.session.cart) {
        return res.redirect('/shopping-cart');
    }
    var cart = new Cart(req.session.cart);
    var stripe = require("stripe")("sk_test_4cqrLAaJ6s2gW1pgwtCAqlnh");
    stripe.charges.create({
        amount: Math.floor(cart.totalPrice) * 100,
        currency: 'usd',
        source: req.body.stripeToken,
        description: 'Test charge'
    }, function (err, charge) {
        if (err) {
            req.flash('error', err.message);
            return res.redirect('/checkout');
        }
        var order = new Order({
            user: req.user,
            address: req.body.address,
            name: req.body.name,
            phone: req.body.phone,
            date: Date.now(),
            received: false,
            cart: cart,
            paymentId: charge.id
        });
        order.save(function (err, result) {
            req.flash('success', 'Successfully bought product. Go to your orders for delivery progress');
            req.session.cart = null;
            res.redirect('/');
        });
    });
});

function isLoggedIn(req, res, next) {
    if (req.isAuthenticated()) {
        return next();
    }
    req.session.oldUrl = req.url;
    res.redirect('/user/signin');
}

function notAdmin(req, res, next) {
    if (req.isAuthenticated()) {
        if (req.user.role === 'admin') {
            return res.redirect('/user/access-restrict');
        }
    }
    return next();
}

function isUser(req, res, next) {
    if (req.isAuthenticated()) {
        if (req.user.role === 'user') {
            return next();
        }
    }

    res.redirect('/access-restrict');
}

function getPlatformList(products) {
    var li = ['All'];
    products.forEach(function (product) {
        if (li.indexOf(product.platform) === -1) {
            li.push(product.platform);
        }
    });
    return li;
}

function getGenreList(products) {
    var li = [];
    products.forEach(function (product) {
        product.genre.forEach(function (g) {
            if (li.indexOf(g) === -1) {
                li.push(g);
            }
        });
    });
    return li;
}


module.exports = router;
